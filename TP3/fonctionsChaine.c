#include "fonctionsChaine.h"

#define MAXCHAR (10)

char * miroir (const char *s){
  char* miroir = malloc(sizeof(*s));
  if(miroir == NULL){
    return NULL;
  }
  int i = 0;
  int tmpTaille = 0;
  for(;*(s + i) != '\0'; i++){
    tmpTaille++;
  }
  tmpTaille--;
  i = 0;
  for(;i <= tmpTaille; i++){
    *(miroir + i) = *(s + tmpTaille - i);
  }
  return miroir;
}

char * saisie (){
  char c;
  char* chaine = malloc(sizeof(MAXCHAR));
  if(chaine == NULL){
    free(chaine);
    return NULL;
  }
  int i = 0;
  printf("\n entrez le mot a stocker ");
  c = getchar();
  while((c!=EOF && !isspace(c))){
    if(i % MAXCHAR == 0)
      chaine = realloc(chaine, i + MAXCHAR );
    if(chaine == NULL){
      free(chaine);
      return NULL;
    }
    *(chaine+i) = c;
    i++;
    c = getchar();
  }
  *(chaine+i) = '\0';
  chaine = realloc(chaine , i+1);
  return chaine;
}
  
