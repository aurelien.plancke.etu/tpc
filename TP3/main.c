#include <stdio.h>
#include <stdlib.h>
#include "fonctionsChaine.h"


int contains(char* controlledString, char charControlled);
int onlyContainsSM(char* controlledString);
int lengthTabPtrChar(char * tab[]);

int main(int argc, char * argv[]){
  argc--;
  int index = 1;
  int flagM = 0;
  int flagS = 0;
  char * tmpS = malloc(sizeof(char)*50);
  for(; index <= argc; index++){
    if(index <= argc && argv[index][0] == '-' && onlyContainsSM(argv[index]) && contains(argv[index], 's')){
      flagS = 1;
    }else if(index <= argc && argv[index][0] == '-' && onlyContainsSM(argv[index]) &&contains(argv[index], 'm')){
      flagM = 1;
    }else if( index == argc && !flagS && flagM){
      tmpS = argv[index];
    }else{
      printf("Mauvaise utilisation\n");
      return 0;
    }
  }
  if(flagM && !flagS){
    printf("%s\n",miroir(tmpS));
  }else if(!flagM && flagS){
    printf("%s\n", saisie());
  }else if(flagM && flagS){
    tmpS = saisie();
    printf("%s\n", miroir(tmpS));
  }
  return 1;
}
  
int contains(char* controlledString, char charControlled){
  int index = 0;
  while(*(controlledString + index) != '\0'){
    if(*(controlledString + index) == charControlled){
      return 1;
    }
    index++;
  }
  return 0;
}

int onlyContainsSM(char* controlledString){
  int index = 0;
  if((*controlledString != '-'))
    return 0;
  index++;
  while(*(controlledString + index) != '\0'){
    if((*(controlledString + index) != 's') && (*(controlledString + index) != 'm') )
      return 0;
    index++;
  }
  return 1;
}

int lengthTabPtrChar(char * tab[]){
  int index = 0;
  while(tab[index] != NULL){
    index++;
  }
  return index;
}
