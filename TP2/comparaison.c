#include "comparaison.h"


int mon_strcmp(const char * s1, const char * s2){
  int i = 0;
  while(s1[i] || s2[i]){
    if(s1[i] < s2[i]){
      return -1;
    }else if (s1[i] > s2[i]){
      return 1;
    }else{
      i++;
    }
  }
  return 0;
}
int mon_strcmpv2(const char * s1, const char * s2, int n){
  int i = 0;
  while( (*(s1+i) || *(s2+i)) && i <= n){
    if(*(s1+i) < *(s2+i)){
      return -1;
    }else if (*(s1+i) > *(s2+i)){
      return 1;
    }
    i++;
  }
  return 0;
}

char *mon_strcat(char *s1, const char *s2){
  int index = 0;
  int index2 =  0;
  while(*(s1+index) ){
    index++;
  }
  while(*(s2+index2)){
    *(s1 +index) = *(s2 + index2);
    index2++;
    index++;
  }
  s1[index] = '\0';
  return s1;
}
