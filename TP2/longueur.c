#include "longueur.h"

int mon_strlen(char s[]){
  int i = 0;
  while(s[i]){
    i++;
  }
  return i;
}

int mon_strlen2(const char *s){
  int i = 0;
  while(*s){
    i++;
    s++;
  }
  return i;
}
