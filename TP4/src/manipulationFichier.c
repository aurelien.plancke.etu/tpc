#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>

#define MAXCHAR (10)

void readF(int fd,int * p_caractere, int * p_mots, int * p_ligne){
  int cpt = 0, reader = 0, estMot = 0;
    int nbOctetALire = 80;
  char * buff = malloc(nbOctetALire);
  if(buff == NULL){
    perror("Le pointeur n'a pas pu être alloué");
    exit(1);
  }
  reader = read(fd,buff,nbOctetALire);
    
  if(reader == -1){
    exit(1);
  }
  while(reader){
    cpt = 0;
    while(cpt != reader){
      if(*(buff+cpt) == '\n'){
	estMot = 0;
	(*p_ligne)++;
      }else if(isspace(*(buff+cpt))){
	estMot = 0;
      }else if(!estMot){
	estMot = 1;
	(*p_mots)++;
      }
      (*p_caractere)++;
      cpt++;
    }
    reader = read(fd,buff,nbOctetALire);
    if(reader == -1){
      exit(1);
    }
  }
  free(buff);
}
  
/*Open the file, read it and count char, words and line*/
void ouverture(char* file,int * p_caractere, int * p_mots, int * p_ligne){
  int fd = 0;
  fd = open(file,O_RDONLY);
  if(fd == -1){
    perror(file);
    exit(1);
  }
  readF(fd,p_caractere,p_mots,p_ligne);
  if(close(fd) == -1){
    perror(file);
    exit(1);
  }
}

void stdinReader(int * p_caractere, int * p_mots, int * p_ligne){
  readF(0,p_caractere,p_mots,p_ligne);
  
}

/*Print the correct use of the command*/
void erreur(){
  printf("Mauvaise utilisation");
  printf("\nSynthaxe : commande options fichiers");
  printf("\n Les options disponibles sont -m pour compter les mots, -c pour compter les caractères et -l pour le nombre ligne");
  printf("\nIl est possible de combiner les options de tel sorte : -mc -mcl -lc ou encore -c -m -l séparés");
  printf("\nLes chemins vers le ou les fichiers doivent toujours se trouver en dernier, ils peuvent être relatif ou absolu");
  exit(1);
}

