#include <stdio.h>
#include <stdlib.h>
#include "../header/manipulationFichier.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>

/*control differents options, recognized by their first char being '-'*/
int containsOnlyValidOption(char * optionsToControl, int * ptr_flagL, int * ptr_flagM, int * ptr_flagC){
  int length = 0;
  while(optionsToControl[length]){
    if(optionsToControl[0] != '-'){
      erreur();
      return 0;
    }
    if((length > 0) &&
       ((optionsToControl[length] != 'l') &&
	(optionsToControl[length] != 'm') &&
       	(optionsToControl[length] != 'c'))) {
      printf("Erreur dans les options !\n");
      erreur();
      return 0;
    }
    if(length > 0 && (optionsToControl[length] == 'l')) *ptr_flagL = 1;
    if(length > 0 && (optionsToControl[length] == 'm')) *ptr_flagM = 1;
    if(length > 0 && (optionsToControl[length] == 'c')) *ptr_flagC = 1;
    length++;
  }
  return 1;
}

/*get all the options*/
void getOptions(int nbArg, char **listArg, int * ptr_flagL, int * ptr_flagM, int * ptr_flagC){
  int cptListArg = 0, indexTabOption = 0, lengthOption = 0;
  while(nbArg != cptListArg){
    if(listArg[cptListArg][0] == '-'){
      if(containsOnlyValidOption(listArg[cptListArg], ptr_flagL, ptr_flagM, ptr_flagC))
	printf("Option : %s",listArg[cptListArg]);
      else{
	printf("\n========ERREUR OPTION");
	erreur();
	exit(0);
      }
    }
    cptListArg++;
  }
}

/* Get all the path to files*/
char ** getPath(int nbArg, char **listArg){
  char **tabFiles = malloc(nbArg * sizeof(char *));
  
  if(!tabFiles) exit(1);
  
  int cptListArg = 0, indexTabFiles = 0, lengthFiles = 0, tmpFD = 0;
  while(nbArg != cptListArg){
   
    if(listArg[cptListArg][0] != '-' && listArg[cptListArg][0] == '.'){
      tmpFD = open(listArg[cptListArg],O_RDONLY);
      if(tmpFD != -1){
	close(tmpFD);
	tabFiles[indexTabFiles] = listArg[cptListArg];
	indexTabFiles++;
      }else{
	free(tabFiles);
	erreur();
	exit(0);
      }
    }
    cptListArg++;
  }
  
  tabFiles = realloc(tabFiles,indexTabFiles*(sizeof(char*)));
  return tabFiles;
}

void printOptions(char ** print){
  int cpt = 0;
  while(print[cpt]){
    printf("option : %s \n",print[cpt]);
    cpt++;
  }
}

void printPath(char ** print){
  int cpt = 0;
  while(print[cpt]){
    printf("Path : %s \n",print[cpt]);
    cpt++;
  }
}
