#include "header/manipulationFichier.h"
#include "header/controleCommande.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc,char** argv){
  if(argc == 2){
    erreur();
    return 1;
  }
  /*argc-- and argv+1 so we don't missuse the executable*/
  argc--;
  int flagL = 0, flagM = 0, flagC = 0;
  getOptions( argc,argv+1,&flagL,&flagM,&flagC);
  
  char ** ptr_listFiles = getPath(argc,argv+1);
  
  if(ptr_listFiles)
    printPath(ptr_listFiles);
  
  int charac = 0,word = 0,line = 0,cptFiles = 0,totalChar = 0, totalLine = 0,totalWord = 0;
  if(ptr_listFiles){
    while(ptr_listFiles[cptFiles]){
      ouverture(ptr_listFiles[cptFiles],&charac,&word,&line);
      printf("\n %s : ",ptr_listFiles[cptFiles]);
      if(flagL) printf("lignes :  %d ",line);
      if(flagM) printf("mots : %d ",word);
      if(flagC) printf("caractères : %d \n",charac);
      totalChar += charac;
      totalLine += line;
      totalWord += word;
      cptFiles++;
      line = 0; word = 0; charac = 0;
    }
  }
    if(argc == 0){
    stdinReader(&charac,&word,&line);
    printf("lignes :  %d ",line);
    printf("mots : %d ",word);
    printf("caractères : %d \n",charac);
    totalChar += charac;
    totalLine += line;
    totalWord += word;
    line = 0; word = 0; charac = 0;
  }
  if(cptFiles>1){
    printf("\n \n Total : ");
    if(flagL) printf("lignes :  %d ",totalLine);
    if(flagM) printf("mots : %d",totalWord);
    if(flagC) printf("caractères : %d \n",totalChar);
  }
  
  free(ptr_listFiles);
  return 0;
}
