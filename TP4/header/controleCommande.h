int containsOnlyValidOption(char * optionsToControl, int * ptr_flagL, int * ptr_flagM, int * ptr_flagC);
char ** getOptions(int nbArg, char **listArg, int * ptr_flagL, int * ptr_flagM, int * ptr_flagC);
char ** getPath(int nbArg, char **listArg);
void printOptions(char ** print);
void printPath(char ** print);
