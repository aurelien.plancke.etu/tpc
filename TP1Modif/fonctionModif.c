#include "fonctionModif.h"

int somme(int liste[], int taille){
  int i = 0;
  int somme = 0;
  while(i < taille){
    somme += liste[i];
    i++;
  }
  return somme;
}

void recopie(int src[], int dest[], int taille){
  int i = 0;
  while(i < taille){
    dest[i] = src[i];
    i++;
      }
}
