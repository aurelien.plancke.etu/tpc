#include "fonctionAffichage.h"

void displaySizeType(){
  char c;
  int i;
  float f;
  double d;
  int t[10];
  char *s="abc";
  struct point {int a; int b;} p;

  printf("\n taille char %ld a l'adresse %p", sizeof(c),&c);
  printf("\n taille int %ld a l'adresse %p", sizeof(i),&i);
  printf("\n taille float %ld a l'adresse %p" , sizeof(f),&f) ;
  printf("\n taille double %ld a l'adresse %p", sizeof(d),&d);
  printf("\n taille int %ld a l'adresse %p", sizeof(t),&t);
  printf("\n taille *char %ld a l'adresse %p", sizeof(s),&s);
  printf("\n taille point %ld a l'adresse %p \n", sizeof(p),&p);
  printf("\n adresse de a %p", &s[0]);
  printf("\n contenu de s %p \n", s);
	      
}

void affiche(int liste[], int taille){
  int i = 0;
  while(i < taille){
    printf("%d ", liste[i]);
    i++;
  }
}
