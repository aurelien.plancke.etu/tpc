#include "fonctionsPoint.h"
#include <math.h>
#include <stdio.h>

/*int pow(int x, int y){
  int a = 1;
  int i = 0;
  while(i < y){
    a *= x;
    i++;
  }
  return a;
  }*/
double distance(point_t p1, point_t p2){
  return sqrt(
	      pow((p1.x - p2.x),2) +
	      pow((p1.y - p2.y),2)
	     );
}

double longueur(point_t segments[], int nombreSegments){
  double sum = 0;
  int cpt = 0;
  while(cpt < nombreSegments){
    sum += (distance(segments[cpt],segments[cpt + 1]));
    cpt++;
  }

  return sum;
}

int plusProche(point_t reference, point_t segments[], int nbSeg){
  int cpt = 0;
  int posPoint = 1;
  float tmpDistance = distance(reference,segments[cpt]);
  cpt++;
  while(cpt <= nbSeg){
    if(distance(reference,segments[cpt]) < tmpDistance){
      tmpDistance = distance(reference,segments[cpt]);
      posPoint = cpt;
    }
    cpt++;
  }
  return posPoint;
}
