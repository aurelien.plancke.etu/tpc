#include "fonctionAffichage.h"
#include "fonctionModif.h"
#include "fonctionsPoint.h"

int main(){
  /*variable declaration*/
  int liste[5];
  int liste2[sizeof(liste)/4];
  int taille = sizeof(liste)/4;
  int i = 0; 
  for(;i < taille; i++){
    liste[i] = i;
  }

  point_t p1,p2,p3,p4;
  point_t tabSeg[3];
  int nbSeg = sizeof(tabSeg)/sizeof(point_t)-1;
  p1.x = 0;p1.y = 0;
  p2.x = 3;p2.y = 5;
  p3.x = 5;p3.y = 10;
  p4.x = -3;p4.y = -2;
  tabSeg[0] = p1;
  tabSeg[1] = p2;
  tabSeg[2] = p3;
  
  displaySizeType();
  affiche(liste, taille);
  printf("\n La somme du tableau est %d \n", somme(liste, taille));
  printf("\n Avant copie : "); affiche(liste2,5);
  recopie(liste, liste2, taille);
  printf("\n Après copie : ");affiche(liste2,5);
  printf("\n La distance entre P1 et P2 est de %f",distance(p1,p2));
  printf("\n La distance totale des points est de %f", longueur(tabSeg, nbSeg));
  printf("\n Le point le plus proche de p4 est p%d", plusProche(p4, tabSeg,nbSeg)+1);
  return 1;
}
